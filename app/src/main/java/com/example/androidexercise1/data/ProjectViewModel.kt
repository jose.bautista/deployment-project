package com.example.androidexercise1.data

import android.app.Application
import android.view.KeyEvent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ProjectViewModel(application: Application) : AndroidViewModel(application) {

    val getProjects: LiveData<List<Projects>>
    private val repository: ProjectRepository

    init {
        val projectsDao = ProjectsDatabase.getDatabase(application).projectsDao()
        repository = ProjectRepository(projectsDao)
        getProjects = repository.getProjects
    }

    fun addProject(project: Projects){
        viewModelScope.launch(Dispatchers.IO){
            repository.addProject(project)
        }
    }

    fun updateProject(project: Projects){
        viewModelScope.launch(Dispatchers.IO){
            repository.updateProject(project)
        }
    }

    fun deleteProject(project: Projects){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteProject(project)
        }
    }

    fun deleteAllProjects(){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteAllProjects()
        }
    }
}