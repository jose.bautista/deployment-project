package com.example.androidexercise1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.androidexercise1.data.ProjectViewModel
import com.example.androidexercise1.data.Projects
import kotlinx.android.synthetic.main.fragment_update2.*
import kotlinx.android.synthetic.main.fragment_update2.view.*


class UpdateFragment : Fragment() {

    private val args by navArgs<UpdateFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_update2, container, false)

        val title  = args.currentProject.title
        val description = args.currentProject.description
        val long_description = args.currentProject.long_description

        view.EditTitle.setText(title)
        view.EditDescription.setText(description)
        view.EditLongDesciption.setText(long_description)

        view.EditButton.setOnClickListener{
            updateItem()
        }

        view.DeleteButton.setOnClickListener{
            deleteItem()
        }

        view.deleteAllButton.setOnClickListener{
            deleteAll()
        }







        return view
    }

    private fun updateItem(){

        val title = EditTitle.text.toString()
        val description = EditDescription.text.toString()
        val long_description = EditLongDesciption.text.toString()

        val updatedProject = Projects(args.currentProject.id, title, description, long_description)

        viewModel.updateProject(updatedProject)
        findNavController().navigate(R.id.action_updateFragment2_to_fragment1)
        Toast.makeText(requireContext(), "Successfully updated!", Toast.LENGTH_LONG).show()


    }

    private fun deleteItem(){

        viewModel.deleteProject(args.currentProject)
        Toast.makeText(requireContext(), "Successfully removed!", Toast.LENGTH_LONG).show()
        findNavController().navigate(R.id.action_updateFragment2_to_fragment1)

    }

    private fun deleteAll(){
        viewModel.deleteAllProjects()
        findNavController().navigate(R.id.action_updateFragment2_to_fragment1)
    }
    private val viewModel: ProjectViewModel by activityViewModels()
}